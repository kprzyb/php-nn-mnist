<?php
// PHP code to show neural network mechanizms. For educational puropse only ! (too many global variables ;))
require_once __DIR__.'\Matrix.php';
use NN\Algebra\Matrix;

$start = microtime(true);
$options = getopt('t:'); //t - test on a model file
$modelFile = null;
if (isset($options['t'])) $modelFile = $options['t'];

srand(make_seed());

$__RAND = function(){return (rand() % 10000 + 1)/10000-0.5;};
$__SIGMOID = function($x){return 1/(1+exp(-$x));};
$__SIGMOIDPRIME = function($x){return exp(-$x)/(pow(1+exp(-$x), 2));};

$__STEPFUNCTION = function($x){
  if($x>=0.5){
    return 1.0;
  }
  if($x<0.5){
    return 0.0;
  }
  return $x;
};

$imageSize = 32;
$numberOfRecords = 946;
$noOfInputNeurons = $imageSize*$imageSize; // 32*32 = 1024 - number of pixels
$noOfHiddenNeurons = 15; // amount of hidden neurons, feel free to experiment
$noOfOutputNeurons = 10; // amount of classes - 10 digits to recognize
$numberOfEpochs = 10; // how many times we iterate through the whole dataset

$data = loadData('data');

$X = null; //input neurons, (32x32 = 1024)

$H = null; //hidden neurons

$W1 = new Matrix($noOfInputNeurons, $noOfHiddenNeurons); //weights between input and hidden neurons

$W2 = new Matrix($noOfHiddenNeurons, $noOfOutputNeurons); //weights between hidden and output neurons

$B1 = new Matrix(1,$noOfHiddenNeurons);//hidden bias

$B2 = new Matrix(1,$noOfOutputNeurons);//output bias

$Y = null; //output neurons / computed output

$Y2 = null; // expected output

$dJdB1 = null; //bias derivative

$dJdB2 = null; //bias derivative

$dJdW1 = null; //weight derivative

$dJdW2 = null; //weight derivative


if ($modelFile){
  restoreModel($modelFile);
}else{
  $W1 = $W1->applyFunction($__RAND);
  $W2 = $W2->applyFunction($__RAND);
  $B1 = $B1->applyFunction($__RAND);
  $B2 = $B2->applyFunction($__RAND);
  //main processing loop
  for($j = 0; $j<$numberOfEpochs; $j++){
    print "Epoch $j\n";
    for ($i=0; $i<$numberOfRecords-20; $i++){
      computeOutput($data[0][$i]);
      computeGradients($data[1][$i]);
    }

  }
  Matrix::printTiming();
  print_r(Matrix::$CALLS);
  storeModel('model_e_'.$numberOfEpochs.'_'.microtime(true).'.txt');
}

//tests
for ($i = $numberOfRecords-20; $i<$numberOfRecords; $i++){
  for ($j=0; $j<10; $j++){
    print $data[1][$i][$j].' ';
  }
  print computeOutput($data[0][$i])->applyFunction($__STEPFUNCTION);
}
$stop = microtime(true);
print "Total time: " . ($stop - $start) . "\n";

//function to push data from the input layer to the output
function computeOutput($input){
  global $X, $H, $Y, $W1, $W2, $B1, $B2, $__SIGMOID;
  $X = new Matrix([$input]);
  $H = $X->dot($W1)->add($B1)->applyFunction($__SIGMOID);
  $Y = $H->dot($W2)->add($B2)->applyFunction($__SIGMOID);
  return $Y;
}

//function that propagates the networ error back by updating weights and biases
function computeGradients($expectedOutput){
  global $X, $W1, $H, $W2, $Y, $B1, $B2, $Y2, $dJdB1, $dJdB2, $dJdW1, $dJdW2, $__SIGMOIDPRIME;
  $learningRate = 0.7;

  $Y2 = new Matrix([$expectedOutput]); // row matrix

  // compute gradients
  $dJdB2 = $Y->sub($Y2)->multiply($H->dot($W2)->add($B2)->applyFunction($__SIGMOIDPRIME));
  $dJdB1 = $dJdB2->dot($W2->transpose())->multiply($X->dot($W1)->add($B1)->applyFunction($__SIGMOIDPRIME));

  $dJdW2 = $H->transpose()->dot($dJdB2);
  $dJdW1 = $X->transpose()->dot($dJdB1);
 
  // update weights
  $W1 = $W1->sub($dJdW1->multiply($learningRate));
  $W2 = $W2->sub($dJdW2->multiply($learningRate));
  $B1 = $B1->sub($dJdB1->multiply($learningRate));
  $B2 = $B2->sub($dJdB2->multiply($learningRate));
}

function loadData($fileName){
  global $imageSize, $numberOfRecords;
  $input = [];
  $output = [];

  $f = fopen($fileName,'r');

  for($i = 0; $i<$numberOfRecords; $i++){ // load 946 examples
    for ($h = 0; $h<$imageSize; $h++){ // read line of 'pixels'
      $line = fgets($f);
      for($w =0; $w<$imageSize; $w++){ // read every pixel
        $input[$i][] = $line[$w];
      }
    }
    $line = fgets($f); //
    $output[$i] = array_fill(0,10,0); //vector length of 10, filled with zeros
    $output[$i][$line[0]] = 1; // index that reprezents a number we just read is being set to 1
  }
  fclose($f);
  return [$input, $output];
}

function storeModel($fileName){
  global $W1, $H, $B1, $W2, $B2;
  $model = array($W1, $H, $B1, $W2, $B2);
  $f = fopen($fileName,'x');
  $s = serialize($model);
  fwrite($f,$s);
  fclose($f);
}

function restoreModel($fileName){
  global $W1, $H, $B1, $W2, $B2;
  $model = unserialize(file_get_contents($fileName));
  list($W1, $H, $B1, $W2, $B2) = $model;
}

function make_seed(){
  list($usec, $sec) = explode(' ', microtime());
  return $sec + $usec * 1000000;
}