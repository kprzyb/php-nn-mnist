<?php

namespace NN\Algebra;

class Matrix
{
  public static $T = array('add'=>0,'sub'=>0,'multiply'=>0, 'multiplyByMatrix'=>0,'dot'=>0,'transpose'=>'0','apply'=>0);
  public static $CALLS = array('add'=>0,'sub'=>0,'multiply'=>0, 'multiplyByMatrix'=>0,'dot'=>0,'transpose'=>'0','apply'=>0);
  public $width;
  public $height;
  public $array;

  public function __construct($par1=1, $par2=1){
    if (is_array($par1)){
      $this->height = count($par1);
      $this->width = count(reset($par1));
      $this->array=$par1;
    }
    else{
      $this->height = $par1;
      $this->width = $par2;
      for($h=0;$h<$this->height;$h++){
        $this->array[]=array_fill(0,$this->width,0);
      }
    }
  }

  public function getShape(){
    return array($this->height,$this->width);
  }

  public function add($matrix){
    $s = microtime(true);
    $assert = ($matrix->height == $this->height) && ($matrix->height == $this->height);

    if (!$assert) throw new \Exception("add: mh:$matrix->height, th:$this->height,mw:$matrix->width, tw:$this->width,");

    $result = [[]];
    for($i = 0; $i<$this->height; $i++){
      for($j = 0; $j<$this->width; $j++){
        $result[$i][$j] = $this->array[$i][$j] + $matrix->array[$i][$j];
      }
    }
    self::$T['add']+= microtime(true) - $s;
    self::$CALLS['add']++;
    return new Matrix($result);
  }

  public function sub($matrix){
    $s = microtime(true);
    $result = [[]];
    for($i = 0; $i<$this->height; $i++){
      for($j = 0; $j<$this->width; $j++){
        $result[$i][$j] = $this->array[$i][$j] - $matrix->array[$i][$j];
      }
    }
    self::$T['sub']+= microtime(true) - $s;
    self::$CALLS['sub']++;
    return new Matrix($result);
  }

  public function multiply($val){
    if(is_object($val)){
      return $this->multiplyByMatrix($val);
    }
    $s = microtime(true);
    $result = [[]];
    for($i = 0; $i<$this->height; $i++){
      for($j = 0; $j<$this->width; $j++){
        $result[$i][$j] = $this->array[$i][$j] * $val;
      }
    }
    self::$T['multiply']+= microtime(true) - $s;
    self::$CALLS['multiply']++;
    return new Matrix($result);
  }

  public function multiplyByMatrix($matrix){
    $s = microtime(true);
    $result = [[]];
    for($i = 0; $i<$this->height; $i++){
      for($j = 0; $j<$this->width; $j++){
        $result[$i][$j] = $this->array[$i][$j] * $matrix->array[$i][$j];
      }
    }
    self::$T['multiplyByMatrix']+= microtime(true) - $s;
    self::$CALLS['multiplyByMatrix']++;
    return new Matrix($result);
  }

  public function dot($matrix){
    $s = microtime(true);
    $assert = ($this->width == $matrix->height);

    if (!$assert) throw new \Exception("dot: mh:$matrix->height != tw:$this->width,");

    $w = 0;
    $result=[[]];
    for($i = 0; $i<$this->height; $i++){
      for($j = 0; $j<$matrix->width; $j++){
        for($h = 0; $h<$this->width; $h++){
          $w += $this->array[$i][$h] * $matrix->array[$h][$j];
        }
        $result[$i][$j] = $w;
        $w=0;
      }
    }
    self::$T['dot']+= microtime(true) - $s;
    self::$CALLS['dot']++;
    return new Matrix($result);
  }

  public function transpose(){
    $s = microtime(true);
    $result = [[]];
    for($i = 0; $i<$this->width; $i++){
      for($j = 0; $j<$this->height; $j++){
        $result[$i][$j] = $this->array[$j][$i];
      }
    }
    self::$T['transpose']+= microtime(true) - $s;
    self::$CALLS['transpose']++;
    return new Matrix($result);
  }

  //work in progres
  public function transposeMulti($array, &$out, $indices = array()){
    if (is_array($array)){
      foreach ($array as $key => $val){
        //push onto the stack of indices
        $temp = $indices;
        $temp[] = $key;
        transpose($val, $out, $temp);
      }
    }
    else{
      //go through the stack in reverse - make the new array
      $ref = &$out;
      foreach (array_reverse($indices) as $idx)
        $ref = &$ref[$idx];
      $ref = $array;
    }
  }


  //work in progres
  public static function readFromPng($imgname){
    $im = new \Imagick();
    $im->readImage($imgname);
    $im->transformImageColorspace(\Imagick::COLORSPACE_GRAY);
    $pixels = $im->exportImagePixels(0, 0, $im->getImageWidth(), $im->getImageHeight(), "B", \Imagick::PIXEL_CHAR);
    return new Matrix(array($pixels));
  }
  //work in progres
  public static function readFromPng1($imgname){
    $im = imagecreatefrompng($imgname);
    imagefilter($im, IMG_FILTER_GRAYSCALE);
    $rgb = imagecolorat($im, 100, 15);
    $r = ($rgb >> 16) & 0xFF;
    $g = ($rgb >> 8) & 0xFF;
    $b = $rgb & 0xFF;
    $width = imagesx($im);
    $height= imagesy($im);
  }

  public function applyFunction($function){
    $s = microtime(true);
    $result = [[]];
    for($i = 0; $i<$this->height; $i++){
      for($j = 0; $j<$this->width; $j++){
        $result[$i][$j] = $function($this->array[$i][$j]);
      }
    }
    self::$T['apply']+= microtime(true) - $s;
    self::$CALLS['apply']++;
    return new Matrix($result);
  }

  //C - row-major, F- column-major
  public function flatten($mode='C'){
    $return = [];
    if ($mode=='C'){
      array_walk_recursive($this->array, function($a) use (&$return) { $return[] = $a; });
      $return = new Matrix(array($return));
    }
    else{
      array_walk_recursive($this->array, function($a) use (&$return) { $return[][] = $a; });
      $return = new Matrix($return);
    }
    return $return;
  }

  public function flattenA(){
    $return = array();
    array_walk_recursive($this->array, function($a) use (&$return) { $return[] = $a; });
    return $return;
  }

  public function unfold($kernelSize){
    $nh = $this->height - $kernelSize +1;
    $nw = $this->width - $kernelSize +1;
    $a=array();
    $rowNum = 0;
    for($i=0; $i < $nh; $i++)
    {
      for($j=0; $j < $nw; $j++)
      {
        $a[$rowNum]=$this->getSlice($i, $i+$kernelSize, $j, $j+$kernelSize)->flattenA();
        $rowNum++;
      }
    }
    return new Matrix($a);
  }

  public function reshape($height,$width){
    $a=[[]];
    $ni=0; $nj=0;
    for($i=0; $i<$this->height; $i++){
      for($j=0; $j<$this->width; $j++){
        $a[$ni][$nj]=$this->array[$i][$j];
        $nj++;
        if ($nj == $width){
          $nj=0;
          $ni++;
        }
      }
    }
    return new Matrix($a);
  }
  //work in progress
  public function convolute($m)  {
    $kernelSize = $m->getShape()[0]; //only square kernels
    $nh = $this->getShape()[0] - $kernelSize +1;
    $nw = $this->getShape()[1] - $kernelSize +1;
    return $this->unfold($kernelSize)->dot($m->flatten('F'))->reshape($nh,$nw);
  }


  public function getSlice($startRow, $stopRow, $startCol, $stopCol){
    $a=[];
    for($i = $startRow; $i < $stopRow; $i++){
      $a[]=array_slice($this->array[$i],$startCol, $stopCol - $startCol);;
    }
    return new Matrix($a);
  }

  //work in progress
  public function rotate90(){
    $mat90 = array();
    for ($i = 0; $i < $this->width; $i++) {
      for ($j = 0; $j < $this->height; $j++) {
        $mat90[$this->height - $i - 1][$j] = $this->array[$this->height - $j - 1][$i];
      }
    }
    return new Matrix($mat90);
  }

  public function __toString(){
    $return = '';
    for($i = 0; $i<$this->height; $i++){
      $return.='|';
      for($j = 0; $j<$this->width; $j++){
        if ($this->array[$i][$j] !== 0 || $this->array[$i][$j] !== 1)
          $r = $this->array[$i][$j];
        else
          $r = number_format($this->array[$i][$j], 2);
        if (strlen($r)>4) $r=substr($r,0,4);
        if (strlen($r) == 1) $r='000'.$r;
        if (strlen($r) == 2) $r='00'.$r;
        if (strlen($r) == 3) $r='0'.$r;
        $return.=$r.'|';
      }
      $return .= PHP_EOL;
    }
    return $return;
  }

  public static function printTiming(){
    $total = 0;
    foreach(self::$T as $method => $time){
      $total+=$time;
    }
    foreach(self::$T as $method => $time){
      print $method.": $time ; ".($time/$total * 100)." %\n";
    }
  }
}
